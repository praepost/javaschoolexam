package com.tsystems.javaschool.tasks.pyramid;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class PyramidBuilder {
    public PyramidBuilder() {

    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */


    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            if (checkAmountOfRows(inputNumbers.size()) == null) throw new CannotBuildPyramidException();
            if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

            //sorting list
            List<Integer> result = inputNumbers.stream()
                    .sorted()
                    .collect(Collectors.toList());

            //build pyramid
            return fillingNumbers(new LinkedList<>(result));

        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }

    public Integer checkAmountOfRows(int listSize) {
        int currentAmount = 1;

        while (listSize > 0) {
            listSize -= currentAmount;
            currentAmount++;
        }

        if (listSize == 0) {
            return currentAmount - 1;

        } else {
            return null;
        }
    }

    public int[][] fillingNumbers(Queue<Integer> inputNumbers) {
        int vertical = checkAmountOfRows(inputNumbers.size());
        int horizontal = vertical * 2 - 1;

        int[][] pyramidArray = new int[vertical][horizontal];

        int globalIndent = horizontal / 2;
        int currentHorizontal = 0;

        while (globalIndent >= 0) {
            int indent = globalIndent;

            for (int i = 0; i <= currentHorizontal; i++) {
                pyramidArray[currentHorizontal][indent] = inputNumbers.poll();
                indent += 2;
            }

            globalIndent--;
            currentHorizontal++;
        }
        return pyramidArray;
    }
}
