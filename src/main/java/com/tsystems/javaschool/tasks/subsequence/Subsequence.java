package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            if (x == null || y == null) {
                throw new IllegalArgumentException();
            }

            List result = (List) y.stream()
                    .filter(v -> x.contains(v))
                    .distinct()
                    .collect(Collectors.toList());

            if (result.equals(x)) {
                return true;
            }

            return false;
        } catch (Exception e){
            throw new IllegalArgumentException();
        }
    }
}
