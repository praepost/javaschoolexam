package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        try {
            //Преобразовываем выражение в постфиксную запись
            String expression = getExpression(statement);
            //Решаем полученное выражение
            double result = counting(expression);

            //Возвращаем строку
            if(Math.ceil(result) == result){
                return String.valueOf((int) result);
            }

            return Double.toString(result);
        } catch (Exception e){
            return null;
        }
    }

    //Метод возвращает true, если проверяемый символ - разделитель ("пробел" или "равно")
    static private boolean isDelimiter(Character c) {
        return c == ' ';
    }

    //Метод возвращает true, если проверяемый символ - оператор
    static private boolean isOperator(Character c) {
        String operations = "+-/*()";

        return operations.contains(c.toString());
    }

    // метод возвращает true, если проверяемый символ - чило или точка
    private static boolean isNumber(Character c){
        String operations = "0123456789.";

        return operations.contains(c.toString());
    }

    //Метод возвращает приоритет оператора
    private static byte getPriority(Character s) {
        switch (s) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
            case '-':
                return 2;
            case '*':
            case '/':
                return 4;
            case '^':
                return 5;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static String getExpression(String input) {

        StringBuilder output = new StringBuilder();
        Stack<Character> operationsStack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            if (isDelimiter(input.charAt(i))){
                output.append(" ");
            } else if (isNumber(input.charAt(i))) {
                while (!isDelimiter(input.charAt(i)) && !isOperator(input.charAt(i))) {
                    output.append(input.charAt(i));
                    i++;

                    if (i == input.length()) {
                        break;
                    }
                }
                output.append(" ");
                i--;
            } else if (isOperator(input.charAt(i))) {
                if (input.charAt(i) == '(')
                    operationsStack.push(input.charAt(i));
                else if (input.charAt(i) == ')') {
                    char s = operationsStack.pop();

                    while (s != '(') {
                        output.append(s).append(" ");
                        s = operationsStack.pop();
                    }
                } else {
                    if (!operationsStack.isEmpty())
                        if (getPriority(input.charAt(i)) <= getPriority(operationsStack.peek()))
                            output.append(operationsStack.pop()).append(" ");

                    operationsStack.push(input.charAt(i));
                }
            }
            if(!isOperator(input.charAt(i)) && !isDelimiter(input.charAt(i)) && !isNumber(input.charAt(i))){
                throw new IllegalArgumentException("how?");
            }
        }

        while (!operationsStack.isEmpty())
            output.append(operationsStack.pop()).append(" ");

        return output.toString();
    }

    private static double counting(String input) {
        double result;
        Stack<Double> temp = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            if (isNumber(input.charAt(i))) {

                StringBuilder a = new StringBuilder();

                while (isNumber(input.charAt(i)) && !isOperator(input.charAt(i))) {
                    a.append(input.charAt(i));
                    i++;
                    if (i == input.length()) break;
                }
                temp.push(Double.parseDouble(a.toString()));
                i--;
            } else if (isOperator(input.charAt(i))) {
                double a = temp.pop();
                double b = temp.pop();

                switch (input.charAt(i)) {
                    case '+':
                        result = b + a;
                        break;
                    case '-':
                        result = b - a;
                        break;
                    case '*':
                        result = b * a;
                        break;
                    case '/':
                        if(Math.ceil(a) == 0) {
                            throw new IllegalArgumentException();
                        }

                        result = b / a;

                        break;
                    case '^':
                        result = Math.pow(b, a);
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
                temp.push(result);
            } else if (!isDelimiter(input.charAt(i))){
                throw new IllegalArgumentException();
            }
        }
        return temp.peek();
    }
}
